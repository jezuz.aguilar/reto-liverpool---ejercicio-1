////// Product Factory
storeModule.factory("ProductFactory", function($http){
	var factory = {};
	var products = [];

	// Obtiene listado de productos
	factory.getProducts = function(callback){
		$http.get("/products").success(function(output){
			products = output;
			callback(products);
		})
	};

	// Solicitud ajax para agregar orden a db
	factory.addProduct = function(info, callback){
		$http.post("/add_product", info).success(function(output){
			console.log(output); 
			callback(output);
		})
	};

	// Obtiene producto por id
	factory.getOneProduct = function(data, callback){
		$http.post('/get_product/'+ data).success(function(data){
			callback(data);
		})
	};

	// Elimina item de db
	factory.removeProduct = function(info, callback) {
		$http.post('/remove_product', info).success(function(output){
			callback(output);
		})
	};
	
	return factory;
});