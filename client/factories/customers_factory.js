////// Customer Factory
storeModule.factory("CustomerFactory", function($http){
	var factory = {};
	var customers = [];

	factory.getCustomers = function(callback){
		$http.get("/customers").success(function(output){
			customers = output;
			callback(customers);
		})
	}

	// Solicitud AJAX para agregar cliente a db
	factory.addCustomer = function(info, callback){
		$http.post("/add_customer", info).success(function(output){
			callback(output);
		})
	};

	factory.deleteCustomer = function(info, callback){
		$http.post("/delete/" + info).success(function(output){
			callback();
		})	
	};

	// Return 
	return factory;
});