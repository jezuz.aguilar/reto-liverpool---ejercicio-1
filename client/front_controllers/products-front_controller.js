////// Products Controller
storeModule.controller("ProductsController", function($scope, ProductFactory){
	$scope.quantity = 5;

	// getProducts: carga productos desde bd
	ProductFactory.getProducts(function(data){
		$scope.products = data;
	})

	// Agregar producto a bd
	$scope.addproduct = function(){
		console.log($scope.new_product);
		ProductFactory.addProduct($scope.new_product, function(info){
			if( info === "A product with this name already exists"){
				$scope.errormsg = info;
			}
			else{
				$scope.errormsg = "";
			}
			ProductFactory.getProducts(function(data){
				$scope.products = data;
				$scope.new_product = {};
			})

			//Reset form
			if (form) {
				$scope.form.$setPristine();
				$scope.form.$setUntouched();
			}
	
		});
	};
});