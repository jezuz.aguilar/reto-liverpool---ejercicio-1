////// One Product Controller
storeModule.controller("OneProductController", function($scope, $routeParams, $location, ProductFactory) {
	
	// llamada por id
	ProductFactory.getOneProduct($routeParams.id, function(data){
		if(data.category == "accessory"){
			data.category = "Accessory";
		} else if (data.category == "shirt"){
			data.category = "Shirt/Coat";
		} else if (data.category == "pant"){
			data.category = "Pants";
		} else if (data.category == "shoe"){
			data.category = "Shoes";
		}
		$scope.product = data;
	});

	// elimina item en bd
	$scope.removeProduct = function(product){
		ProductFactory.removeProduct(product, function() {
			// Redirect a productos
			$location.path("/products");
		})
	}
})