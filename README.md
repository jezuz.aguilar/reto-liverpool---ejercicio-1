# Reto Liverpool Ejercicio 1

Resumen: Reto Liverpool (Ejercicio 1) se construye utilizando MEAN Stack. 
Los usuarios son administradores de la tienda y pueden agregar y eliminar productos, clientes y pedidos de la base de datos. 
Los usuarios también pueden filtrar los productos, clientes y pedidos que se muestran. 
Todos los cambios se muestran a través de AngularJS. 
Los formularios incluyen la validación angular front-end, así como la validación del servidor back-end para garantizar que no se envíen duplicados a la base de datos.


## Instalación

Para empezar, simplemente hay que clonar este repositorio e instalar las dependencias.

>git clone 

>npm install

>npm start

## Tecnologías
* AngularJS
* Node.js
* Express.js
* Bootstrap
* MongoDB