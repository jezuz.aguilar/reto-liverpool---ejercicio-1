// /server/models/product.js

var mongoose = require('mongoose');

// Crear ProductSchema
var ProductSchema = new mongoose.Schema({
	name: String,
	url: String,
	desc: String,
	quantity: Number,
	category: String,
	createdAt: {type:Date, default: Date.now}
});

mongoose.model('Product', ProductSchema);