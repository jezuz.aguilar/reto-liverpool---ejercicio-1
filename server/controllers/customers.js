// /server/controllers/customers.js

// Requiere mongoose y modelo de Cliente
var mongoose = require("mongoose");
var Customer = mongoose.model("Customer");

module.exports = (function(){
	return{
		// Trater todos los clientes de db
		show: function(req, res){
			Customer.find({}, function(error, results){
				if(error){
					console.log(error);
				} else {
					res.json(results);
				}
			})
		},

		// Agregar nuevos clientes
		add: function(req, res) {
			// Crear nuevo modelo
			var newCustomer = new Customer(req.body);

			// Si cliente ya existe no agregar nuevamente a db
			Customer.findOne({name: req.body.name}, function(error, response){ 
				if(response){ // cliente existe
					console.log("El cliente ya existe");
					res.send("El cliente con ese nombre ya existe.");
				}
				else{
					// guardar
					newCustomer.save(function(error, results){
						if(error){
							console.log("No se guardó el cliente");
						} else {
							console.log("El cliente se guardó correctamente");
							res.json(results);
						}
					});
				}
			})
		},

		// Eliminar cliente
		delete: function(req, res) {
			console.log("customers.js _id value: " , req.params.id);
			Customer.remove({_id: req.params.id}, function(error, results){
				if(error){
					console.log("Error al aliminar cliente");
				} else {
					console.log("Cliente eliminado satisfactoriamente");
					res.json(results);
				}
			}) 
		},
	}
})();