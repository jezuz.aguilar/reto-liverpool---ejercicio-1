// /server/controllers/orders.js

// Requiere mongoose y modelo de Pedido
var mongoose = require("mongoose");
var Customer = mongoose.model("Customer");
var Order = mongoose.model("Order");
var Product = mongoose.model("Product");

module.exports = (function(){
	return{

		// Obtiene todas las ordenes
		orders: function(req, res){
			Order.find({}, function(error, results){
				if(error){
					console.log(error);
				} else {
					res.json(results);
				}
			})
		},

		// Obtiene clientes
		all_customers: function(req, res){
			Customer.find({}, function(error, results){
				if(error){
					console.log(error);
				} else {
					res.json(results);
				}
			})
		},

		// Obtiene productos
		products: function(req, res){
			Product.find({}, function(error, results){
				if(error){
					console.log(error);
				} else {
					res.json(results);
				}
			})
		},

		// Agrega nuevo pedido
		order: function(req, res){
			var newOrder = new Order(req.body);
			
			// Encuentra cantidad
			Product.findOne({name: req.body.product}, function(error, product){ 
				var diff = product.quantity - req.body.quantity;

				// If db quantity - req.body.quantity > 0
				if( diff > 0){ 
					Product.update({name: req.body.product}, {quantity: diff}, function(error, product){ // update quantity in db
						console.log("Cantidad actualizada");
						// Call .save function
						newOrder.save(function(error, results){
							if(error){
								console.log("No se guardó pedido");
							} else {
								console.log("Pedido guardado satisfactoriamente");
								res.json(results);
							}
						});
					});
				} else {
					console.log("No se puede comprar por falta de stock");
					res.send("No se puede comprar por falta de stock.");
				}
			})		
		},

		// Eliminar pedido
		remove: function(req, res){
			console.log("order values: " , req.params);
			Order.remove({_id: req.params.id}, function(error, results){
				if(error){
					console.log("Error al eliminar pedido");
				} else {
					console.log("Pedido eliminado satisfactoriamente");
					res.json(results);
				}
			}) 
		}
	}
})();