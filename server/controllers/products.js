// /server/controllers/products.js

// Requiere mongoose y modelo de Producto
var mongoose = require("mongoose");
var Product = mongoose.model("Product");

module.exports = (function(){
	return{

		// Obtiene productos
		products: function(req, res){
			Product.find({}, function(error, results){
				if(error){
					console.log(error);
				} else {
					res.json(results);
				}
			})
		},

		// Agrega producto
		add: function(req, res){
			var newProduct = new Product(req.body);
			console.log("Controlador backend:", req.body);
			Product.findOne({name: req.body.name}, function(error, response){ 
				if(response){ //producto existe
					console.log("El producto ya existe");
					res.send("El producto ya existe");
				}
				else{
					// Guardar
					newProduct.save(function(error, results){
						if(error){
							console.log("No se guardó");
						} else {
							console.log("producto guardado satisfactoiamente");
							res.json(results);
						}
					});
				}
			})
		},

		remove: function(req, res) {
			Product.remove({_id: req.body._id}, function(err, response) {
				if(err) {
					console.log('El producto no fue eliminado.');
				}
				else {
					console.log("Producto eliminado satisfactoriamente.");
					res.json(response);
				}
			})
		},

		getOneProduct: function(req, res){
			Product.find({_id: req.params.id}, function(err, data){
				if(err)
				{
					console.log("Error.");
				} else {
					console.log("Hecho.")
					res.json(data[0]);
				}
			})
		}
	}
})();